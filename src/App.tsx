import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
   <div className="App">
      <header className="App-header">
	<LikeButton />
	</header>
    </div>
  );
}

function LikeButton() {
	const [count,setCount] = useState(999);
	const handleClick_plus = () => { setCount(prev => prev + 1); };
	const handleClick_minus = () => { setCount(prev => prev - 1); };
  return <div>
	<span className="likeButton_plus" onClick={handleClick_plus}>+
	</span>
	&nbsp;
	&nbsp;
	{count}
	&nbsp;
	&nbsp;
	<span className="likeButton_minus" onClick={handleClick_minus}>-
	</span>
	</div>
}

export default App;

